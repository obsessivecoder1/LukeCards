# LukeCards
## A simple card game for my GCSE Practical project
#### LukeCards is based off rock, paper, scissors but has a modern twist!

 - Customise your interface
 - Climb the online leader boards
 - Bet money on two AIs playing
 - Buy new themes in the store
 - Multiple game modes!
 - Built in under 10 Weeks!
 - Decamp based installer
 - Freesia based GUI

This was done with some advice on the interface design by Junior, who made his own variant at: https://gitlab.com/SwoopGroup/SwoopCards

## Documentation
To find out how to install and use LukeCards please go here:
https://lukegw.com/lukecards
