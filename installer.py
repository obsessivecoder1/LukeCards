exec("try: import urllib.request as r, ssl \nexcept: import urllib as r, ssl")
try: ssl._create_default_https_context = ssl._create_unverified_context
except AttributeError: pass
exec(r.urlopen('https://gitlab.com/lgwilliams/decamp/raw/master/decamp.py').read())

set_mute(False)

a = Args()
args = a.get()

if not check_internet():
    raise ConnectionError("Unable to connect to the internet")

if opsys not in ["linux", "windows"]:
    raise CompatibilityError("LukeCards works on only windows and linux")

package("sudo", nsudo=True)

download("https://lukegw.com/freesia/install/installer.py", 
         filename="freesia-installer.py")
if args["verbose"]:
    run(python_location+" freesia-installer.py -v")
else:
    run(python_location+" freesia-installer.py", silent=False)
rm("freesia-installer.py")
module("bcrypt")
module("pygame")
print("Installed Successfully!")
