"""LukeCards - A simple card game
Written by Luke Williams in 2018
LukeCards is under the LGPL License"""
from contextlib import contextmanager
from freesia import *
import itertools
import sqlite3
import bcrypt
import random
import pprint
import string
import time
import ast
import sys
import os

@contextmanager
def shutup():
    """Makes pygame shut up"""
    o, sys.stdout = sys.stdout, open(os.devnull, "w")
    try: yield open(os.devnull, "w")
    finally: sys.stdout = o
        
with shutup():
    from pygame import mixer

#Initialize the mixer for pygame (frequency=44100)
mixer.init(44100)

#Initialize the music tracks
global click
click = mixer.Sound("./sound/click.wav")

#The design of the program
app = {"theme":('#1d3557', '#457b9d', '#a8dadc', '#f1faee'),
       "font":"ubuntu",
       "titlesize":"50px",
       "subtitlesize":"30px",
       "textsize":"15px",
       "style":"platique",
       "fg":"black"}

#Add the delete button color to the theme
app["theme"] += ("red",)

#The credits
credits = """
LukeCards is licensed under LGPL V3 (See LICENSE for details)
Thanks to:
 - Luke Williams (https://lukegw.com)
 - The Qt Company for PySide, Qt4 and Qt5 (https://qt.io)
 - Python programming language (https://python.org)
 - Gitlab for version management (https://gitlab.com)
"""

#Setup the coordinates for the grid for the card preview in calssic mode
grid = [(i/10, 0.05) for i in range(1, 10)]
grid += [(i/10, 0.12) for i in range(1, 10)]
grid += [(i/10, 0.19) for i in range(1, 10)]
grid.append((0.5, 0.3))

if sys.version_info.major == 2:
    input = raw_input

def old_unicode(text):
    """Backwards compatibility"""
    try: return str(text, "utf8")
    except: return str(text)

#If the major python version is 3, create a python 2 unicode function
if sys.version_info.major == 3:
    unicode = old_unicode

class DatabaseError(Exception):
    """An exception called when a database error occurs"""
    pass

#Create a database file
file = sqlite3.connect("main.db")

#Get the cursor
crs = file.cursor()

#The SQL for creating a table
#Contains a password, username and ID for speedy login
sql_create_table = """
CREATE TABLE IF NOT EXISTS main (
    userid integer,
    username text NOT NULL,
    password text NOT NULL,
    currency integer);
"""
#If the database doesn't exist, create it
if file is not None:
    crs.execute(sql_create_table)
else:
    raise DatabaseError("Unable to create database")
    
def verify(username, password):
    """Confirm a users password"""
    try:
        #Get the database, userid and password and compare the hashes
        database = get()
        userid = [(i[1]) for i in get()].index(username)
        pwhash = database[userid][2].encode("utf8")
        username = database[userid][1]
        return bcrypt.checkpw(password.encode("utf8"), pwhash)    
    except:
        return False
    
def verify_username(username):
    """Ensure the username is valid"""
    #Ensure the name is not too long
    if len(username) > 20: return False, 0
    #Ensure the name is not too short
    if len(username) < 2: return False, 1
    #Ensure the name is not already in the database
    if username in [i[1] for i in get()]: return False, 2
    return True, 3

def verify_password(password):
    """Verify that the password strength is over the score of 9"""
    if password_strength(password) > 9:
        return True
    else:
        return False

def password_strength(password):
    """Ensure the username is valid"""
    score = 0
    for i in password:
        #Add a point if they contain unusual characters
        if i in string.punctuation: score += 1
        if i in string.ascii_uppercase: score += 0.5
        if i in string.digits: score += 0.5
    #Add the length of the password to the score
    score += len(password)
    return score

def add(username, password):
    """Add an entry to the table"""
    #Verify that username is valid
    if not verify_username(username)[0]: raise DatabaseError("Invalid username")
    if not verify_password(password): raise DatabaseError("Invalid password")
    #Hash the password, salt it for extra security
    password = bcrypt.hashpw(password.encode("utf8"), bcrypt.gensalt())
    #Find the biggest userid in the table to find the userid
    if get() == []: userid = 0
    else: userid = [i[0] for i in get()][-1]+1
    #Organize and cast the data
    sqldata = [unicode(userid), unicode(username), unicode(password), unicode(100)]
    #Insert into table
    crs.execute("""INSERT INTO main (userid, username, password, currency)
VALUES ('"""+"', '".join(sqldata)+"');")
    #Remove data for security reasons
    del password, username, userid, sqldata
    #Write the changes
    file.commit()
    
def view():
    """View the SQL database"""
    pprint.pprint(get())
    
def get():
    """Get the contents of the database"""
    return list(crs.execute("SELECT * FROM main"))

def currency_give(username, amount):
    """Give the user some currency"""
    try: userid = [(i[1]) for i in get()].index(username)
    except: return
    if get_currency(username) < 0:
        raise
    curcur = get_currency(username) + amount
    crs.execute("""UPDATE main
    SET currency='"""+str(curcur)+"""'
    WHERE userid='"""+unicode(userid)+"';")
    file.commit()
    
def get_currency(username):
    """Find the amount of currency the user has"""
    userid = [(i[1]) for i in get()].index(username)
    return int(get()[userid][3])

def remove(username, password):
    """Remove a user"""
    #Verify password for security reasons
    database = get()
    userid = [(i[1]) for i in get()].index(username)
    pwhash = database[userid][2].encode("utf8")
    username = database[userid][1]
    if bcrypt.checkpw(password.encode("utf8"), pwhash): 
        #Actually delete the user from the database
        crs.execute("""DELETE FROM main
        WHERE userid='"""+unicode(userid)+"';")
    else: raise DatabaseError("Password is Incorrect")
    #Write to database
    file.commit()
    
def change_username(username, password, newusername):
    """Change the users name in the database"""
    #Verify password
    if not verify(username, password): raise DatabaseError("Incorrect details")
    if not verify_username(newusername)[0]: raise DatabaseError("Invalid new username") 
    #Get the users ID
    db = get()
    userid = [(i[1]) for i in db].index(username)
    crs.execute("""UPDATE main
    SET username='"""+newusername+"""'
    WHERE userid='"""+unicode(userid)+"';")    
    #Save changes
    file.commit()
    
def change_password(username, password, newpassword):
    """Change the users password in the database"""
    #Verify password
    if not verify(username, password): raise DatabaseError("Incorrect details")
    if not verify_password(newpassword): raise DatabaseError("Invalid new password") 
    #Get the users ID
    db = get()
    userid = [(i[1]) for i in db].index(username)
    newpassword = bcrypt.hashpw(newpassword.encode("utf8"), bcrypt.gensalt())
    crs.execute("""UPDATE main
    SET password='"""+unicode(newpassword)+"""'
    WHERE userid='"""+unicode(userid)+"';")
    #Save changes
    file.commit()
    
def close():
    """Close the db file"""
    #Close the cursor
    crs.close()
    #Close the file
    file.close()

#The acutal game mechanics
class Game():
    def __init__(self, AI=False):
        #Define outcomes
        self.AI = AI
        self.outcomes = {"rock, paper":"paper",
                         "rock, scissors":"rock",
                         "scissors, rock":"rock",
                         "scissors, paper":"scissors",
                         "paper, rock":"paper",
                         "paper, scissors":"scissors"}
        self.run()
    def run(self):
        #Create cards
        self.cards = list(itertools.product(["rock", "paper", "scissors"], 
                                       [str(i) for i in range(1, 11)]))
        #Shuffle cards
        random.shuffle(self.cards)
        #Setup players
        self.player1 = []
        self.player2 = []
        if self.AI: self.gennedname = [self.genname(), self.genname()]
        else: self.gennedname = self.genname()
    def genname(self):
        """Randomly generate a name"""
        number = str(random.randint(0, 100))
        names = ["Noah","William","James","Logan","Benjamin","Mason",
                 "Elijah","Oliver","Jacob","Lucas","Michael","Alexander",
                 "Ethan","Daniel","Matthew","Aiden","Henry","Joseph",
                 "Jackson","Samuel","Sebastian","David","Carter","Wyatt",
                 "John","Owen","Dylan"]
        name, name2  = random.choice(names), random.choice(names)
        nformat = random.randint(1, 5)
        nname = {1:name+number, 2:name, 3:name+number+name2, 
                 4:name+name2+number, 5:name+name2}[nformat]
        return nname
    def newround(self):
        """Start a new round"""
        if not self.cards:
            if len(self.player1) > len(self.player2): return 1
            elif len(self.player2) > len(self.player1): return 2            
        self.player1_card, self.player2_card = self.draw_cards()    
    def draw_cards(self):
        """Draw the cards from the deck"""
        self.player1_card = self.cards.pop(0)
        self.player2_card = self.cards.pop(0)
        return self.player1_card, self.player2_card
    def determine_winner(self):
        """Determine winner"""
        if self.player1_card[0] == self.player2_card[0]:
            if int(self.player1_card[1]) > int(self.player2_card[1]): 
                self.winner = 1
            else: self.winner = 2
        else:
            cardcode = self.player1_card[0]+", "+self.player2_card[0]
            if self.player1_card[0] == self.outcomes[cardcode]:
                self.winner = 1
            else: self.winner = 2
        if self.winner == 1: 
            self.player1.append(self.player1_card)
            self.player1.append(self.player2_card)
        else:
            self.player2.append(self.player1_card)
            self.player2.append(self.player2_card)            
        return self.winner
    
def c2u(user, player, winner):
    """Get the player name from a number"""
    if winner == 1: return player
    else: return user


#More advanced file writing
class dio():
    def __init__(self, data):
        self.data = data
    def __getitem__(self, value):
        return self.values()[self.keys().index(value)]
    def __setitem__(self, key, value):
        if key in self.keys():
            self.data[self.keys().index(key)] = (key, value)
        else:
            self.data.append((key, value))
    def __delitem__(self, key):
        del self.data[self.keys().index(key)]
    def __len__(self):
        return len(self.data)
    def keys(self):
        return [i[0] for i in self.data]
    def values(self):
        return [i[1] for i in self.data]
    def items(self):
        return [(k, v) for k, v in self.data]
    def get(self, key):
        return self[key]

class AFW():
    def __init__(self, filename):
        """Advance file writer"""
        self.filename = filename
        if not os.path.exists(filename):
            fwrite = open(self.filename, "w")
            fwrite.write("[]")
            fwrite.close()
    def read(self, index=None):
        """read from file"""
        fread = open(self.filename, "r")
        res = dio(ast.literal_eval(str(fread.read())))
        if index is None:
            return res
        else:
            return (list(res.keys())[index], res[list(res.keys())[index]])
    def delete(self, slot):
        """delete from file"""
        data = self.read()
        del data[slot]
        self.writeall(data)
    def writeall(self, data):
        """overwrite the file"""
        self.reset()
        for k, v in list(data.items()):
            self.write(k, v)
    def write(self, slot, what):
        """append to the file"""
        data = self.read()
        data[slot] = what
        fwrite = open(self.filename, "w")
        fwrite.write(str(data.data))
        fwrite.close()
    def reset(self):
        """wipe the file"""
        fwrite = open(self.filename, "w")
        fwrite.write("[]")
        fwrite.close()
    def sort(self):
        """Sort the dictionary by keys (timsort algorithm)"""
        d = self.read()
        s = dio([(i, d[i]) for i in sorted(d.keys(), key=d.get)])
        self.writeall(s)
    def rename(self, old, new):
        """rename a slot"""
        contents = self.read()[old]
        self.delete(old)
        self.write(new, contents)
    def reversesort(self):
        d = self.read()
        s = dio([(i, d[i]) for i in sorted(d.keys(), key=d.get, reverse=True)])
        self.writeall(s)

#Import the leaderboard
leaderboard = AFW("data/local-leaderboard.txt")

#Import the settings file
settings = AFW("data/settings")
if "mute" not in settings.read().keys():
    settings.write("mute", False)

#A class that handles the displaying of cards
class Card():
    def __init__(self, cardtype, number, x=0, y=0, opp=False, prev=False):
        #Create a grey background
        self.body = Frame(root, bg="grey")
        #Add an image to the card
        self.pixmap = Image(self.body, path="images/"+cardtype+".png")
        if not prev: 
            self.pixmap.config(size=(200, 300))
            self.pixmap.place(0.5, 0.5)
        else:
            self.pixmap.config(size=(45, 45))
            self.pixmap.place(0.3, 0.5)
        #Add a label to the card
        self.label = Label(self.body, text=str(number),
                           font=app["font"], fg="black", autorefresh=False)
        self.label.setAlignment(Center)
        if not prev:
            self.label.config(fontsize=app["titlesize"])
            self.label.place(0.5, 0.9, 60, 60, anchor="S")
        else:
            self.label.config(fontsize=app["textsize"])
            self.label.place(0.75, 0.5, 20, 20)
        #Add the click to reveal guide
        self.guide = Label(self.body, text="Click to reveal",
                           fontsize=app["subtitlesize"],
                           font=app["font"], fg="black")
        self.guide.place(0.5, 0.5)
        #Place the card on the screen
        if not prev:
            self.body.place(x, y, 200, 300, rounded=True, radius=40)
        else:
            self.body.place(x, y, 80, 40, rounded=True, radius=20)
        #If this is not the opponents card, hide the guide
        if not opp:
            self.pixmap.hide()
            self.label.hide()
        else:
            self.guide.hide()
        #If this is a preview card, hide the guide
        if prev:
            self.guide.hide()
            self.pixmap.show()
            self.label.show()
    def reveal(self):
        """reveal the cards contents"""
        click_sound()
        self.pixmap.show()
        self.label.show()
        self.guide.hide()

def show_credits():
    """Display the credits"""
    click_sound()
    showInformationMessage(root, title="LukeCards Credits", 
                           text=credits)

def exit_gui():
    """Quit the program"""
    click_sound()
    root.hide()
    exitMainloop()
    quit()

def hide_all():
    """Hide all widgets"""
    for i in allWidgets:
        i.hide()
    name_entry.clear()
    password_entry.clear()
    confirm_password_entry.clear()
    
def show_menu():
    """Show the login and signup menu"""
    hide_all()
    click_sound()
    #Reset the bindings for all buttons
    back_button.bind("<Button-1>", show_menu)
    delete_account_button.bind("<Button-1>", blank)
    changeusername_button.bind("<Button-1>", blank)
    changepassword_button.bind("<Button-1>", blank)
    #Show the widgets
    title.show()
    subtitle.show()
    login_button.show()
    signup_button.show()
    credits_button.show()
    exit_button.show()
    
def show_login_menu():
    """Show the login menu"""
    hide_all()
    click_sound()
    back_button.show()
    name_entry.show()
    password_entry.show()
    name_label.show()
    password_label.show()
    submit_button_login.show()
    logo_image.show()
    
def show_signup_menu():
    """Show the sign up menu"""
    hide_all()
    click_sound()
    name_label.show()
    password_label.show()
    confirm_password_label.show()
    confirm_password_entry.show()
    back_button.show()
    name_entry.show()
    submit_button_signup.show()
    password_entry.show()
    logo_image.show()

def show_account_settings():
    """Show the account settings menu"""
    hide_all()
    click_sound()
    mute_switch.show()
    delete_account_button.show()
    changeusername_button.show()
    changepassword_button.show()
    back_button.show()

def export_game(user, cards):
    """Export a game to the local leaderboard"""
    #The pickle module is garbage so behold:
    if user not in leaderboard.read().keys():
        leaderboard.write(user, cards)
    elif cards > leaderboard.read()[user]:
        leaderboard.write(user, cards)
    leaderboard.sort()
        
def delete_leaderboard(user):
    """Delete a user from the leaderboard"""
    if user not in leaderboard.read().keys(): return
    leaderboard.delete(user)
    leaderboard.sort()

def edit_leaderbaord(user, newuser):
    """Rename a user"""
    if user not in leaderboard.read().keys(): return
    leaderboard.rename(user, newuser)
    leaderboard.sort()

def play_betting(user):
    """Play the betting mode"""
    #Verify that the user has money
    if get_currency(user) <= 0:
        #Show error and exit betting mode
        showErrorMessage(root, "No cash!", "You have no coins to bet with!")
        return
    hide_all()
    currency_viewer.show()
    play_music(2)
    #create a new betting game with AI
    game1 = Game(AI=True)
    #show the player names
    p1label = Label(root, text=game1.gennedname[0], fg="white", 
                     font=app["font"], fontsize=app["subtitlesize"])
    p2label = Label(root, text=game1.gennedname[1], fg="white", 
                     font=app["font"], fontsize=app["subtitlesize"])
    p1label.place(0.25, 0.8)
    p2label.place(0.75, 0.8)
    #Show the score labels
    score1.config(text="Score: "+str(len(game1.player1)))
    score2.config(text="Score: "+str(len(game1.player2)))        
    score1.show()
    score2.show()
    #Ask the user which person to bet on
    beton = None
    amount = get_currency(user)*10000
    while beton not in ["1", "2"]:
        #Keep asking the user until they enter a valid number
        beton = getInput(root, "Enter player name", "Who do you wish to bet on?\
        \nFor player: "+game1.gennedname[0]+", enter '1'\
        \nFor player: "+game1.gennedname[1]+", enter '2'")[0]
    #Ask the user how much to bet
    while True:
        #Keep asking the player until they select a number under 1001 and over 0
        amount = int(getInput(root, "Amount", "Please enter the amount you wish to spend:\
        \nMust be under 1000 and over 0")[0])
        if amount <= get_currency(user) and amount in list(range(1, 1001)):
            break
    #remove the amount from the users account
    currency_give(user, -1*amount)
    #run the rounds until one of them wins
    def runround():
        """Run the betting round"""
        def clean(winnerlabel):
            """clear the screen"""
            winnerlabel.hide()
            del winnerlabel
        click_sound()
        status = game1.newround()
        if status in [1, 2]: 
            game1.overallwinner = status
            onwin()
            return
        game1.determine_winner()
        score1.config(text="Score: "+str(len(game1.player1)))
        score2.config(text="Score: "+str(len(game1.player2)))
        card = Card(*game1.player1_card, opp=True, x=0.25, y=0.5)
        card1 = Card(*game1.player2_card, opp=True, x=0.75, y=0.5)
        winner = c2u(*reversed(game1.gennedname), game1.winner)
        winnerlabel = Label(root, text="Winner: "+winner, 
                            fontsize=app["subtitlesize"],
                            fg="white", font=app["font"])
        winnerlabel.place(0.5, 0.9, anchor="S")
        ExecAfter(4000, runround)
        ExecAfter(4000, lambda: clean(winnerlabel))
    def onwin():
        """When the game ends"""
        #If they bet correctly give them double
        if beton == str(game1.overallwinner):
            #If you win, display message
            showInformationMessage(root, "You win", "You bet correctly!\
            \nTherefore you get: "+str(int(amount*2)))
            #Give them double
            currency_give(user, int(amount*2))
        else:
            #If you lose, display message
            showInformationMessage(root, "Incorrect bet!", 
                                   "You bet incorrectly\
            \nTherefore you do not get your money back :-(")
        #Go back to main menu
        play_music(0)
        show_main_screen()
    #Start the round after 2000 milliseconds (2 seconds)
    ExecAfter(2000, runround)
    
def play_classic():
    """Begin to play the classic version"""
    hide_all()
    click_sound()
    play_music(1)
    global game1, usr, youlabel, opplabel
    #Create a new game
    game1 = Game()
    #Get a randomly generated name
    usr = game1.gennedname
    #Create labels for the users
    youlabel = Label(root, text=user, fg="white", font=app["font"], 
                     fontsize=app["subtitlesize"])
    opplabel = Label(root, text=usr, fg="white", font=app["font"], 
                     fontsize=app["subtitlesize"])
    youlabel.place(0.25, 0.8)
    opplabel.place(0.75, 0.8)
    #Show the score labels
    score1.config(text="Score: "+str(len(game1.player1)))
    score2.config(text="Score: "+str(len(game1.player2)))      
    score1.show()
    score2.show()
    #Run the classic game
    run(user)
    
def finish_classic(user, score, stat):
    """When the classic game finishes"""
    #Export the game to the leaderboard
    export_game(user, score)
    if stat == 1:
        #If player 1 (human) wins
        #Give them 10 coins
        currency_give(user, 10)
        #Display message
        showInformationMessage(root, title="You won!", 
                               text="Well done on winning\n+10 coins")
    else:
        #Display message
        showInformationMessage(root, title="You lost!", 
                               text="Argh better luck next time!")
    #Go back to the account screen
    play_music(0)
    show_main_screen()    

def blank():
    """Do Nothing"""
    pass

def run(user):
    """Run a new round"""
    #Draw cards
    status = game1.newround()
    #Update the score in the labels
    score1.config(text="Score: "+str(len(game1.player1)))
    score2.config(text="Score: "+str(len(game1.player2)))
    if status is None:
        #If the deck is still full
        #Show card
        def reveal(card, oppcard):
            """When the user clicks the card"""
            #Reset the binding
            card.body.bind("<Button-1>", blank)
            #Actually show the card
            card.reveal()
            #Play sound
            click_sound()
            #Get the winner and show the opponents card
            ExecAfter(2000, oppcard.body.show)
            ExecAfter(4000, getwinner)
        #Create human card
        card = Card(*game1.player1_card, x=0.25, y=0.5)
        #Create computer card
        card1 = Card(*game1.player2_card, x=0.75, y=0.5, opp=True)
        card1.body.hide()
        #Bind the card so when it is clicked, run the reveal function
        card.body.bind("<Button-1>", lambda: reveal(card, card1))
        def clean(card1, card, winnerlabel):
            """Clean up for the next game"""
            card1.body.hide()
            card.body.hide()
            winnerlabel.hide()
            del card, card1, winnerlabel
            ExecAfter(0, lambda: run(user))
        def getwinner():
            """Show the winner"""
            #Get the winner
            game1.determine_winner()
            #Show the card preview
            for c, i in enumerate(game1.player1):
                inventory = Card(*i, prev=True, x=grid[c][0], y=grid[c][1])
                inventory.body.bind("<Button-1>", blank)
            #Convert the number to the username
            winner = c2u(usr, user, game1.winner)
            #Display the winner in a label
            winnerlabel = Label(root, text="Winner: "+winner, 
                                fontsize=app["subtitlesize"],
                                fg="white", font=app["font"])
            winnerlabel.place(0.5, 0.9, anchor="S")
            #Update the score labels
            score1.config(text="Score: "+str(len(game1.player1)))
            score2.config(text="Score: "+str(len(game1.player2)))   
            #Clean up
            ExecAfter(2000, lambda: clean(card1, card, winnerlabel))
    else:
        #If the deck is empty, finish the classic game
        finish_classic(user, len(game1.player1), status)
        
def change_username_front(username):
    """The frontend for the change username"""
    click_sound()
    #Ask the user to confirm their password
    currentpwd = getInput(root, "", "Enter your password", password=True)[0]
    if not verify(username, currentpwd): 
        showErrorMessage(root, "Incorrect password", "Incorrect password")
        return
    #Ask and verify the users desired username
    newusr = getInput(root, "", "Enter your new username: ")[0]
    confnewusr = getInput(root, "", "Confirm your new username: ")[0]
    if newusr == confnewusr:
        #Verify that the usernames match
        try:
            change_username(username, currentpwd, newusr)
        except DatabaseError:
            #If there is a validation error
            showInformationMessage(root, "", "Invalid username")
            return        
    else:
        #Display a message if the passwords do not match
        showInformationMessage(root, "Confusion", "Usernames do not match!")
        return
    #Rename the user on the leaderboard
    edit_leaderbaord(username, newusr)
    #Show a message
    showInformationMessage(root, "Complete", "Username changed")
    #Log them out
    log_out()

def change_password_front(username):
    """Frontend for changing the users password"""
    click_sound()
    #Ask the user to confirm their password
    currentpwd = getInput(root, "", "Enter your password", password=True)[0]
    #If the password is correct
    if not verify(username, currentpwd): 
        #Show an error if the password is incorrect
        showErrorMessage(root, "Incorrect password", "Incorrect password")
        return
    #Ask the user to enter and confirm their desired password
    newpwd = getInput(root, "", "Enter your new password: ", password=True)[0]
    confnewpwd = getInput(root, "", "Confirm your new password: ", password=True)[0]
    if newpwd == confnewpwd: 
        try:
            #Change they match
            change_password(username, currentpwd, newpwd)
        except DatabaseError:
            #Display an error message if the password is invalid
            showInformationMessage(root, "", "Invalid password")
            return
    else:
        #Display a message of the passwords do not match
        showInformationMessage(root, "Confusion", "Passwords do not match!")
        return
    #Show a message if the password was changed
    showInformationMessage(root, "Complete", "Password changed")
    #Log out
    log_out()

def log_out():
    """Log the user out of their account"""
    click_sound()
    show_menu()

def update_currency():
    """Update the currency label"""
    cur = get_currency(user)
    currency_viewer.config(text="Coins: "+str(cur))

def delete_account(user):
    """The frontend for account deletion"""
    click_sound()
    #Show a warning
    showWarningMessage(root, title="WARNING!", 
                       text="YOU ARE ABOUT TO DELETE YOUR ACCOUNT!")
    #Vonfirm the users username
    username = getInput(root, title="Confirmation", 
                        text="Enter your username to PERMANENTLY DELETE \
                        \nyour account")[0]
    if username != user:
        #Cancel the deletion if the username is incorrect of canel is pressed
        showInformationMessage(root, title="Cancelled", 
                               text="Deletion cancelled")
        return
    #Ask them to enter their password to confirm
    backup = getInput(root, title="Confirmation", 
                      text="Please enter your password to confirm:")[0]
    if not verify(user, backup):
        #If the password is incorrect
        showInformationMessage(root, title="Cancelled", 
                               text="Deletion cancelled")
        return
    #Remove the account
    remove(user, backup)
    #Remove them from the leaderboard
    delete_leaderboard(user)
    #Logout and show a message
    log_out()
    showInformationMessage(root, title="Goodbye!", text="Account has been deleted")

def update_leader_list():
    """Update the leaderboard list widget"""
    leaderboard_list.clear()
    for k, v in leaderboard.read().items():
        leaderboard_list.insert(0, str(k)+" - "+str(v))

def show_main_screen():
    """Show the menu that lists the game modes"""
    hide_all()
    leaderboard.sort()
    click_sound()
    update_leader_list()
    leaderboard_list.show()
    play_classic_button.show()
    play_betting_button.show()
    leaderboard_label.show()
    logout_button.show()
    account_button.show()
    update_currency()
    currency_viewer.show()
    logo_image.show()
    #Bind the back button to show the accoutn screen
    back_button.bind("<Button-1>", show_main_screen)
    
def submit_login():
    """GUI Login Process"""
    #Get the contents of the text boxes
    username = name_entry.text()
    password = password_entry.text()
    #Verify the users credentials
    if verify(username, password):
        #Set up the current users username and log in
        global user
        user = username
        #Bind the account settings
        delete_account_button.bind("<Button-1>", lambda: delete_account(user))
        changeusername_button.bind("<Button-1>", lambda: change_username_front(user))
        changepassword_button.bind("<Button-1>", lambda: change_password_front(user))
        show_main_screen()
    else: 
        #Show an error message
        showErrorMessage(root, title="Incorrect details", 
                         text="Incorrect details!")

def mute_toggle():
    """Mute or unmute the music and sound effects"""
    mute = settings.read()["mute"]
    if mute: settings.write("mute", False)
    else: settings.write("mute", True)
    mute_switch.config(text={True:"Unmute", False:"Mute"}[settings.read()["mute"]])
    play_music(0)

def click_sound():
    """Play a click sound"""
    #If mute, dont play
    if settings.read()["mute"]: return    
    click.stop()
    click.play()

def play_music(mode):
    """Play music"""
    mixer.music.stop()
    time.sleep(0.5)
    #If mute, dont play
    if settings.read()["mute"]: return
    if mode == 0:
        #Menu music
        mixer.music.load("sound/menu.mp3")
    elif mode == 1:
        #Classic game mode
        mixer.music.load("sound/classic.mp3")
    elif mode == 2:
        #Betting game mode
        mixer.music.load("sound/betting.mp3")
    mixer.music.play(loops=-1)

def add_animation(widget, theme=2):
    """Add animation to the button"""
    widget.bind("<Enter>", lambda: widget.config(bg=app["theme"][1]))
    widget.bind("<Leave>", lambda: widget.config(bg=app["theme"][theme]))

def submit_signup():
    """Gui login process"""
    #Get the contents of the text boxes
    username = name_entry.text()
    password = password_entry.text()
    confirm_password = confirm_password_entry.text()
    #If the username is not valid, display an error message
    if not verify_username(username)[0]:
        reason = {0:"Username is over 20 characters",
                  1:"Username is under 2 characters",
                  2:"Username already exists"}
        showErrorMessage(root, title="Invalid username!", 
                         text=reason[verify_username(username)[1]])
        return
    #If the passwords do not match, show an error message
    if not verify_password(password):
        passwd_text = "Please add symbols and extra characters to your password \
        \nto make it stronger"
        showErrorMessage(root, title="Invalid password!", 
                         text=passwd_text)
        return
    if password != confirm_password:
        showErrorMessage(root, title="Password confusion!",
                         text="Passwords do not match!")
        return
    #Add the credentials to the database
    add(username, password)
    show_menu()

#Create a window
root = Window(title="LukeCards", icon="icon.png", bg=app["theme"][0], 
              autoshow=False)
#Style the application
setApplicationStyle(app["style"])
#Resize and place the window
root.geometry(1000, 700, center=True)

#Create welcome labels
title = Label(root, text="Welcome to LukeCards!", font=app["font"], 
              fontsize=app["titlesize"], fg="white")
title.place(0.5, 0.1)

subtitle = Label(root, text="The simple card game", font=app["font"], 
                 fontsize=app["subtitlesize"], fg="white")
subtitle.place(0.5, 0.2)

#Create the option buttons
login_button = Button(root, text="Login", bg=app["theme"][2], 
                      font=app["font"], fg=app["fg"], 
                      fontsize=app["subtitlesize"])
login_button.place(0.333333, 0.4, 300, 200)

signup_button = Button(root, text="Sign up", bg=app["theme"][2], 
                       font=app["font"], fg=app["fg"], 
                       fontsize=app["subtitlesize"])
signup_button.place(0.666666, 0.4, 300, 200)

credits_button = Button(root, text="Credits", bg=app["theme"][2], 
                       font=app["font"], fg=app["fg"],
                       fontsize=app["subtitlesize"])
credits_button.place(0.333333, 0.75, 300, 200)

exit_button = Button(root, text="Exit", bg=app["theme"][2], 
                       font=app["font"], fg=app["fg"], 
                       fontsize=app["subtitlesize"])
exit_button.place(0.666666, 0.75, 300, 200)

#Create the back button
back_button = Button(root, text="< Back", bg=app["theme"][2], 
                       font=app["font"], fg=app["fg"], 
                       fontsize=app["subtitlesize"])
back_button.place(0.01, 0.01, anchor="NW")

#Create the credential boxes
name_entry = Entry(root, bg="black", fg="white", 
                   fontsize=app["subtitlesize"])
name_entry.place(0.6, 0.45)

password_entry = Entry(root, mode="password", bg="black", 
                       fg="white", fontsize=app["subtitlesize"])
password_entry.place(0.6, 0.55)

confirm_password_entry = Entry(root, bg="black", mode="password",
                       fg="white", fontsize=app["subtitlesize"])
confirm_password_entry.place(0.6, 0.65)

#Create the labels for the credential boxes
name_label = Label(root, text="Username: ", font=app["font"], 
                   fontsize=app["subtitlesize"])
name_label.place(0.42, 0.45, anchor="W")

password_label = Label(root, text="Password: ", font=app["font"], 
                       fontsize=app["subtitlesize"])
password_label.place(0.42, 0.55, anchor="W")

confirm_password_label = Label(root, text="Confirm Password: ", font=app["font"], 
                               fontsize=app["subtitlesize"])
confirm_password_label.place(0.42, 0.65, anchor="W")

#Create the submit button
submit_button_login = Button(root, text="Submit >", font=app["font"],
                             fontsize=app["subtitlesize"], bg=app["theme"][3],
                             fg="black")
submit_button_login.place(0.5, 0.7)

submit_button_signup = Button(root, text="Submit >", font=app["font"],
                             fontsize=app["subtitlesize"], bg=app["theme"][3],
                             fg="black")
submit_button_signup.place(0.5, 0.8)

#Create the play game menu buttons
play_classic_button = Button(root, text="Play classic game", font=app["font"],
                     fg="black", bg=app["theme"][2], fontsize=app["titlesize"])
play_classic_button.place(0.5, 0.45, 500)

play_betting_button = Button(root, text="Play betting game", font=app["font"],
                     fg="black", bg=app["theme"][2], fontsize=app["titlesize"])
play_betting_button.place(0.5, 0.6, 500)

currency_viewer = Label(root, text="", fg="white", font=app["font"], 
                        fontsize=app["subtitlesize"])
currency_viewer.place(0.01, 0.01, anchor="NW")

score1 = Label(root, text="Score: 0", fontsize=app["subtitlesize"],
               fg="white", font=app["font"])
score1.place(0.25, 0.28, anchor="S")

score2 = Label(root, text="Score: 0", fontsize=app["subtitlesize"],
               fg="white", font=app["font"])
score2.place(0.75, 0.28, anchor="S")

account_button = Button(root, text="Account settings", font=app["font"],
                     fg="black", bg=app["theme"][3], fontsize=app["subtitlesize"])
account_button.place(0.99, 0.01, anchor="NE")

delete_account_button = Button(root, text="Delete account", font=app["font"],
                     fg="black", bg="red", fontsize=app["subtitlesize"])
delete_account_button.place(0.5, 0.2, 400)

changeusername_button = Button(root, text="Change username", font=app["font"],
                     fg="black", bg=app["theme"][3], fontsize=app["subtitlesize"])
changeusername_button.place(0.5, 0.3, 400)

changepassword_button = Button(root, text="Change password", font=app["font"],
                     fg="black", bg=app["theme"][3], fontsize=app["subtitlesize"])
changepassword_button.place(0.5, 0.4, 400)

logout_button = Button(root, text="Log out", font=app["font"],
                     fg="black", bg=app["theme"][3], fontsize=app["subtitlesize"])
logout_button.place(0.99, 0.08, anchor="NE")

mute_switch = Button(root, text={True:"Unmute", False:"Mute"}[settings.read()["mute"]],
                     font=app["font"], fg="black", bg=app["theme"][3], 
                     fontsize=app["subtitlesize"])
mute_switch.place(0.5, 0.5, 400)

logo_image = Image(root, path="images/icon.png", size=(262, 222))
logo_image.place(0.5, 0.2)

leaderboard_list = List(root, selectionbg="white", selectionfg="black")
leaderboard_list.place(0.1, 0.55, 150, 600)

leaderboard_label = Label(root, text="Leaderboard:", fontsize=app["textsize"],
                          fg="white", font=app["font"])
leaderboard_label.place(0.1, 0.1)

#When the user clicks the widget, run the fucntion
login_button.bind("<Button-1>", show_login_menu)
signup_button.bind("<Button-1>", show_signup_menu)
credits_button.bind("<Button-1>", show_credits)
exit_button.bind("<Button-1>", exit_gui)
back_button.bind("<Button-1>", show_menu)
submit_button_login.bind("<Button-1>", submit_login)
submit_button_signup.bind("<Button-1>", submit_signup)
play_classic_button.bind("<Button-1>", play_classic)
play_betting_button.bind("<Button-1>", lambda: play_betting(user))
account_button.bind("<Button-1>", show_account_settings)
logout_button.bind("<Button-1>", log_out)
mute_switch.bind("<Button-1>", mute_toggle)

#Add animations
add_animation(login_button)
add_animation(signup_button)
add_animation(credits_button)
add_animation(exit_button)

add_animation(back_button)
add_animation(submit_button_signup)
add_animation(submit_button_login)

add_animation(play_classic_button)
add_animation(play_betting_button)
add_animation(account_button, theme=3)
add_animation(logout_button, theme=3)

add_animation(delete_account_button, theme=4)
add_animation(changeusername_button, theme=3)
add_animation(changepassword_button, theme=3)
add_animation(mute_switch, theme=3)

#Show the menu
show_menu()

#Play the menu music
play_music(0)

#Show the rendered window and start the program
root.show()
mainloop()

#Secure the database to prevent editing after the program is closed
close()

#After the game is finished, stop all music
mixer.music.stop()